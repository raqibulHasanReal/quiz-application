<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionSet;

class ExaminationController extends Controller
{
    public function index(){
        $questions = QuestionSet::get();
        return view('examination.index')->with(compact('questions'));
    }
}
