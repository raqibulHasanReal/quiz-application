<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionSet;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class QuestionSetController extends Controller
{
    public function store(Request $request) {
        try{
            DB::beginTransaction();
            $questionSet = new QuestionSet;
            $questionSet->question = $request->input('question');
            $questionSet->option_1 = $request->input('option_1');
            $questionSet->option_2 = $request->input('option_2');
            $questionSet->option_3 = $request->input('option_3');
            $questionSet->option_4 = $request->input('option_4');
            $questionSet->answer = $request->input('answer');
            $questionSet->made_by = Auth::user()->name;
            $questionSet->save();

            DB::commit();

            return redirect()->back()->with('success', 'You are create new question..!!');

        } catch (\Exception $errors) {
            DB::rollBack();
            return response(['error' => $errors->getMessage()], 500);
            return redirect()->back()->with('error', 'There are some errors..!!');
        }
    }
}
