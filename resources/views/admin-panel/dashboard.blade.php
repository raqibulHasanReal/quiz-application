@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form method="post" action="{{ route('question.store') }}">
                        @csrf
                        @method('POST')
                        <div class="form-group row">
                            <label for="question" class="col-sm-2 col-form-label">Question</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="question" name="question" placeholder="Question" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="option_1" class="col-sm-2 col-form-label">Option 1</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_1" name="option_1" placeholder="Option 1" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="option_2" class="col-sm-2 col-form-label">Option 2</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_2" name="option_2" placeholder="Option 2" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="option_3" class="col-sm-2 col-form-label">Option 3</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_3" name="option_3" placeholder="Option 3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="option_4" class="col-sm-2 col-form-label">Option 4</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="option_4" name="option_4" placeholder="Option 4">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="answer" class="col-sm-2 col-form-label">Answer</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="answer" name="answer" placeholder="Answer" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 offset-sm-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
