@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Examination</div>

                <div class="card-body">
                    <form>
                        @foreach ($questions as $question_set)

                        <p>{{$question_set->id}} . <span>{{$question_set->question}}</span></p>
                        <div class="pl-3 pr-3 pb-3">
                                <input type="radio" name="optradio[<?php echo $question_set->id; ?>]" value="{{$question_set->option_1}}"> <label for="">{{$question_set->option_1}}</label><br>
                                <input type="radio" name="optradio[<?php echo $question_set->id; ?>]" value="{{$question_set->option_2}}"> <label for="">{{$question_set->option_2}}</label><br>
                                @if(!$question_set->option_3 == null)
                                    <input type="radio" name="optradio[<?php echo $question_set->id; ?>]" value="{{$question_set->option_3}}"> <label for="">{{$question_set->option_3}}</label><br>
                                @endif
                                @if(!$question_set->option_4 == null)
                                    <input type="radio" name="optradio[<?php echo $question_set->id; ?>]" value="{{$question_set->option_4}}"> <label for="">{{$question_set->option_4}}</label><br>
                                @endif
                                <input  type="text" name="ques_id" value="{{$question_set->id}}" hidden>
                        </div>
                        @endforeach
                        <button class="btn btn-primary" type="submit">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
